from pathlib import Path
import json
import pandas as pd
import numpy as np
from  sklearn.preprocessing import normalize, robust_scale
from sklearn.cluster import k_means
from sklearn.decomposition import PCA
from sklearn.svm import OneClassSVM
from matplotlib import pyplot as plt
from sklearn.inspection import DecisionBoundaryDisplay
import seaborn as sns


# Part 1: Extract the compensated TS for each target
data_dir = "data/BroadbandTS_T20240321_02182641-20240321_02330400_track03.json"
for path in Path(data_dir).glob("*.json"):
    if "track03" in str(path):
        with open(path, "r") as f:
            data = json.load(f)
        break

records = []
for ping in data['pings']:
    channels = ping.pop('channels')
    for channel in channels:
        targets = channel.pop('targets')
        for target in targets:
            target.update(channel)
            target.update(ping)
            records.append(target)

df = pd.DataFrame.from_records(records)
features = np.stack(df['tsc'])  # The feature vectors for each sample are defined here

# Part 2: Check statistics
fig, ax = plt.subplots(1, 2)
sns.histplot(df, x='nominalFrequency', ax=ax[0])
sns.histplot(df, x='range', ax=ax[1])

# Part 3: Simple machine learning approaches (PCA reduction and K-means)
features_n = normalize(features, axis=0, norm="max")  # first normalize the features
# features_n = robust_scale(features, axis=0)  # first standardize the features

# 3A) Try reducing the features with PCA
components = 6
required_explained_var = 0.99
P = PCA(components)  # 6 components seems to work for this data
P.fit(features_n)
ndims = np.argmax(P.explained_variance_ratio_.cumsum() > required_explained_var)  # Further reduce data so that explained variance is > 99%
tformed = P.transform(features_n)  # Transform the data into the PCA space
reduced = tformed[:, :ndims+1]  # Reduce the data

# 3B) Try clustering with k-Means on the reduced data
clusters = 3
coords, labels, fitness = k_means(reduced, clusters, random_state=42)  # Cluster the reduced data

fig = plt.figure()
ax = fig.add_subplot(projection='3d')
ax.set_title("Clustered on reduced feature vectors.")
ax.scatter(reduced[:, 0], reduced[:, 1], reduced[:, 2], c=labels,)

# 3C) Try clustering with k-Means on the complete data
coords_full, labels_full, fitness_full = k_means(features_n, clusters, random_state=42)
fig = plt.figure()
ax = fig.add_subplot(projection='3d')
ax.set_title("Clustered on complete feature vectors.")
ax.scatter(reduced[:, 0], reduced[:, 1], reduced[:, 2], c=labels_full,)

# Part 4: Inspect the statistics for the labels
df['label'] = labels_full
fig, ax = plt.subplots()
sns.histplot(df, x='range', hue='label', ax=ax, multiple='stack', palette="viridis")

X = df['range'] * np.sin(np.deg2rad(df['alongshipAngle'])) * np.cos(np.deg2rad(df['athwartshipAngle']))
Y = df['range'] * np.sin(np.deg2rad(df['alongshipAngle'])) * np.sin(np.deg2rad(df['athwartshipAngle']))
Z = df['range'] * np.cos(np.deg2rad(df['alongshipAngle']))

fig = plt.figure()
ax = fig.add_subplot(projection='3d')
ax.scatter(X, Y, Z, c=df['label'])
ax.set_xlabel('X (m)')
ax.set_ylabel('Y (m)')
ax.set_zlabel('Z (m)')
ax.set_zlim(Z.max(), Z.min())

# Part 5: SVM Outlier Detection
clf = OneClassSVM()
clf.fit(features_n)
y_pred = clf.predict(features_n)

fig = plt.figure()
ax = fig.add_subplot(projection='3d')
ax.scatter(X, Y, Z, c=y_pred)
ax.set_xlabel('X (m)')
ax.set_ylabel('Y (m)')
ax.set_zlabel('Z (m)')
ax.set_zlim(Z.max(), Z.min())

# Part 6: PyTorch Auto-Encoder
import torch
from torch import nn
from torch.utils.data import Dataset, DataLoader

if torch.cuda.is_available():
    print("CUDA is available. GPU Acceleration can be used.")
else:
    print("CUDA is not available. Training will be performed on CPU.")

latent_space = 6


class Encoder(nn.Module):
    def __init__(self):
        super(Encoder, self).__init__()
        self.net = nn.Sequential(
            nn.Linear(63, 32),
            nn.Tanh(),
            nn.Linear(32, 16),
            nn.Tanh(),
            nn.Linear(16, latent_space),
            nn.Tanh()
        )

    def forward(self, x):
        return self.net(x)


class Decoder(nn.Module):
    def __init__(self):
        super(Decoder, self).__init__()
        self.net = nn.Sequential(
            nn.Linear(latent_space, 16),
            nn.Tanh(),
            nn.Linear(16, 32),
            nn.Tanh(),
            nn.Linear(32, 63),
            nn.Tanh()
        )

    def forward(self, x):
        return self.net(x)


class Autoencoder(nn.Module):
    def __init__(self):
        super(Autoencoder, self).__init__()
        self.encoder = Encoder()
        self.decoder = Decoder()

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x

    def encode(self, x):
        # This method will only run the encoder and return the latent vector
        return self.encoder(x)


model = Autoencoder()
criterion = nn.MSELoss()
optimizer = torch.optim.Adam(model.parameters(), lr=1e-5)


def train(model, criterion, optimizer, data_loader, epochs):
    for epoch in range(epochs):
        for data in data_loader:
            # Assuming input data is loaded as (batch_size, 63)
            inputs = data

            # Zero the parameter gradients
            optimizer.zero_grad()

            # Forward pass
            outputs = model(inputs)
            loss = criterion(outputs, inputs)

            # Backward pass and optimize
            loss.backward()
            optimizer.step()

        print(f'Epoch [{epoch + 1}/{epochs}], Loss: {loss.item():.6f}')


class CustomDataset(Dataset):
    def __init__(self, data):
        # Initialization where data is a PyTorch Tensor
        self.data = data

    def __len__(self):
        # Return the size of the dataset
        return len(self.data)

    def __getitem__(self, index):
        # Fetch the item (tensor slice) by index
        return self.data[index]


features_n_tensor = torch.tensor(features_n, dtype=torch.float32)

dataset = CustomDataset(features_n_tensor)

# Create the DataLoader
data_loader = DataLoader(dataset, batch_size=64, shuffle=True)

# Example: Iterating through the DataLoader
for batch in data_loader:
    # Each 'batch' is now a batch of 64 instances from your dataset
    print(batch.shape)  # Outputs: torch.Size([64, 63])
    break  # We use 'break' just to print the first batch and not loop over all data

# Train the model
train(model, criterion, optimizer, data_loader, epochs=200)

# Extract the latent representation

def extract_latent_vectors(model, data_loader):
    model.eval()  # Set the model to evaluation mode
    latent_vectors = []

    with torch.no_grad():  # No need to track gradients for this operation
        for data in data_loader:
            latent_vec = model.encode(data)
            latent_vectors.append(latent_vec)

    # Concatenate all batches into a single tensor
    latent_vectors = torch.cat(latent_vectors, dim=0)
    return latent_vectors

# Assuming 'data_loader' is already created and contains your dataset
latent_vectors = extract_latent_vectors(model, data_loader)

# Convert to numpy array if necessary
latent_vectors_numpy = latent_vectors.numpy()

# Cluster on the latent space
coords_ae, labels_ae, fitness_ae = k_means(latent_vectors_numpy, clusters, random_state=42)  # Cluster the reduced data

fig = plt.figure()
ax = fig.add_subplot(projection='3d')
ax.scatter(reduced[:, 0], reduced[:, 1], reduced[:, 2], c=labels_ae,)

# Inspect the label versus range
df['label_ae'] = labels_ae
fig, ax = plt.subplots()
sns.histplot(df, x='range', hue='label_ae', ax=ax, multiple='stack', palette="viridis")

fig = plt.figure()
ax = fig.add_subplot(projection='3d')
ax.scatter(X, Y, Z, c=labels_ae)
ax.set_xlabel('X (m)')
ax.set_ylabel('Y (m)')
ax.set_zlabel('Z (m)')
ax.set_zlim(Z.max(), Z.min())